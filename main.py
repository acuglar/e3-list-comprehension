def list_comprehension_exercise_1():
    return [i for i in range(11)]


def list_comprehension_exercise_2():
    return [i for i in range(22) if i % 2 == 0 or i % 3 == 0]


def list_comprehension_exercise_3():
    return [i for i in range(-5, 32) if i % 2 != 0 and i % 5 != 0]


def list_comprehension_exercise_4():
    return [i ** 2 for i in range(11)]


def list_comprehension_exercise_5(sentence: str):
    return [i for i in sentence if i == i.upper() and i != ' ']


def list_comprehension_exercise_6(sentence: str):
    return [i for i in sentence.split() if i[0] == 'r' and len(i) >= 4]